
import draughts.*;

import java.awt.*;
import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

public class ModelTests {

    private class TestPlayer implements Player {

        @Override
        public Move notify(Set<Move> moves) {
            if (moves.iterator().hasNext()) return moves.iterator().next();
            return null;
        }

    }

    @Test
    public void testCorrectPieceIsReturned() {
        Set<Piece> pieces = new HashSet<Piece>();
        Piece piece = new Piece(Colour.Red, 3, 5);
        pieces.add(piece);

        DraughtsModel model = new DraughtsModel("Test", null, Colour.Red, pieces);

        assertEquals("The correct piece should be returned", piece, model.getPiece(3, 5));
    }


    public class TestModel extends DraughtsModel {

      public TestModel(String gameName, Player player, Colour currentPlayer, Set<Piece> pieces) {
          super(gameName, player, currentPlayer, pieces);
      }

      public TestModel(String gameName, Player player) {
          super(gameName, player);
      }

      public boolean removePieceInModel(Point position, Point destination) {
          return removePiece(position, destination);
      }

      public void turnInModel() {
          turn();
      }

      public void playInModel(Move move) {
          play(move);
      }


    }

    @Test
    public void testGameNameIsCorrect() throws Exception {
        DraughtsModel model = new DraughtsModel("Test", null);

        assertEquals("The game name should be the same as the one passed in", "Test", model.getGameName());
    }

    @Test
    public void testCurrentPlayerIsRedAtStartOfGame() throws Exception {
        DraughtsModel model = new DraughtsModel("Game", null);

        assertEquals("The red player should be the current player at the beginning of the game.", Colour.Red, model.getCurrentPlayer());
    }

    @Test
    public void testCurrentPlayerUpdatesCorrectly() throws Exception {
        TestModel model = new TestModel("Test", new TestPlayer());

        assertEquals("The current player should be red initially", Colour.Red, model.getCurrentPlayer());

        model.turnInModel();
        assertEquals("The current player should be white after one turn", Colour.White, model.getCurrentPlayer());

        model.turnInModel();
        assertEquals("The current player should be red after two turns", Colour.Red, model.getCurrentPlayer());
    }


    @Test
    public void testRemovePieceWorksCorrectly() throws Exception {

        TestModel model = new TestModel("Test", new TestPlayer());
        Point pos = new Point(3,5);
        Point dest = new Point(5,3);

        assertEquals("Should have jumped over a piece", Boolean.TRUE, model.removePieceInModel(pos, dest));
    }


    @Test
    public void testBasicPlayRunsCorrectly() throws Exception {
        TestModel model = new TestModel("Test", new TestPlayer());
        Piece piece = new Piece(Colour.Red, 2, 2);
        Move move = new Move(piece, 3, 3);
        model.playInModel(move);
        assertEquals("Should have same X value", 3, piece.getX());
        assertEquals("Should have same Y value", 3, piece.getY());

        move = new Move(piece, 2, 4);
        model.playInModel(move);
        assertEquals("Should have same X value", 2, piece.getX());
        assertEquals("Should have same Y value", 4, piece.getY());
    }

    @Test
    public void testJumpPlayRunsCorrectly() throws Exception {
        Set<Piece> pieces = new HashSet<Piece>();
        Piece piece1 = new Piece(Colour.Red, 3, 5);
        Piece piece2 = new Piece(Colour.White, 4, 6);
        pieces.add(piece1);
        pieces.add(piece2);
        TestModel model = new TestModel("Test", new TestPlayer());
        Move move = new Move(piece1, 5, 7);
        model.playInModel(move);
        assertEquals("Should jump over and have X value", 5, piece1.getX());
        assertEquals("Should jump over and have y value", 7, piece1.getY());

        assertEquals("Should have removed a piece", Boolean.FALSE, pieces.contains(piece2));


    }
    @Test
    public void testInitialisePiecesWorks() throws Exception {
        Set<Piece> pieces = new HashSet<Piece>();
        DraughtsModel model = new DraughtsModel("Test", null, Colour.Red, pieces);
        if(model.getPiece(0,1) !=null) {
            assertEquals("Piece should be at (0,1)", Colour.White, model.getPiece(0,1).getColour());
        }
        else {
            assertEquals("No piece on first location", 1, 2);
        };
    }
}
